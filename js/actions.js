export default [
    {
        "lathe": {
            "axis6": "83.40",//6轴
            "axis3": "0.00",//3轴
            "axis2": "0.00",//2轴
            "axis1": "-5.00"//1轴-导轨
        },
        "hole": {
            "cylinder": "0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null

    },
    {
        "lathe": {
            "axis6": "83.40",//6轴
            "axis3": "0.00",//3轴
            "axis2": "0.00",//2轴
            "axis1": "-8.00"//1轴-导轨
        },
        "hole": {
            "cylinder": "0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "83.40",//6轴
            "axis3": "0.00",//3轴
            "axis2": "0.00",//2轴
            "axis1": "-11.90"//1轴-导轨
        },
        "hole": {
            "cylinder": "0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "83.40",//6轴
            "axis3": "0.00",//3轴
            "axis2": "0.00",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "83.40",//6轴
            "axis3": "0.00",//3轴
            "axis2": "5.10",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "83.40",//6轴
            "axis3": "0.00",//3轴
            "axis2": "5.10",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "83.40",//6轴
            "axis3": "-3.40",//3轴
            "axis2": "5.10",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "83.40",//6轴
            "axis3": "-7.29995422363281",//3轴
            "axis2": "5.10",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "80.40",//6轴
            "axis3": "-7.29995422363281",//3轴
            "axis2": "5.10",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "77.00",//6轴
            "axis3": "-7.29995422363281",//3轴
            "axis2": "5.10",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "74.10",//6轴
            "axis3": "-7.29995422363281",//3轴
            "axis2": "5.10",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "74.10",//6轴
            "axis3": "-7.29995422363281",//3轴
            "axis2": "5.10",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "74.10",//6轴
            "axis3": "-7.29995422363281",//3轴
            "axis2": "5.10",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "-0.10"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "77.50",//6轴
            "axis3": "-7.29995422363281",//3轴
            "axis2": "5.10",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "-0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "80.50",//6轴
            "axis3": "-7.29995422363281",//3轴
            "axis2": "5.10",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "-0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
    {
        "lathe": {
            "axis6": "83.40",//6轴
            "axis3": "-7.29995422363281",//3轴
            "axis2": "5.10",//2轴
            "axis1": "-13.92777099609375"//1轴-导轨
        },
        "hole": {
            "cylinder": "-0.20"//柱体
        },
        "line": [//折线图
            150, 230, 224, 218, 135, 147, 260
        ],
        "rough": null
    },
]