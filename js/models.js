export default [
    {
        id: 1,
        icon: 'icon-machine',
        text: '机床',
        modelName: 'lathe',
        url: "./model/jichuang.gltf",
        // url: "http://114.115.165.12:8084/model/loadFileAsId/2461",
        modelSize: 1.45,
        modelType: 'gltf',
        loadType: 'userModel',
        scale: { x: 10, y: 10, z: 10 },
        position: { x: 20, y: 0, z: 0 },
        rotation: { x: 0, y: 0, z: 0 },
        userData: {

        },
    },
    {
        id: 2,
        icon: 'icon-machine',
        text: '飞机',
        modelName: 'plane',
        url: "./model/plane.gltf",
        // url: "http://114.115.165.12:8084/model/loadFileAsId/2461",
        modelSize: 1.45,

        modelType: 'gltf',
        loadType: 'userModel',
        scale: { x: 5, y: 5, z: 5 },
        position: { x: -32, y: 0, z: -7 },
        rotation: { x: 0, y: 0, z: 0 },

        userData: {

        },
    },
    {
        id: 3,
        icon: 'icon-machine',
        text: '开孔',
        modelName: 'open',
        url: "./model/kaikong.gltf",
        // url: "http://114.115.165.12:8084/model/loadFileAsId/2461",
        modelSize: 1.45,
        modelType: 'gltf',
        loadType: 'userModel',
        scale: { x: 1, y: 1, z: 1 },
        position: { x: -0.32, y: 19.65, z: -20.8 },
        rotation: { x: -1.6, y: 0, z: 0 },

        userData: {

        },
    }
]